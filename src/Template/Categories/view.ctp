<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $category
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Editer la catégorie'), ['action' => 'edit', $category->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Supprimer la catégorie'), ['action' => 'delete', $category->id], ['confirm' => __('Êtes-vous sûr de supprimer la catégorie # {0}?', $category->id)]) ?> </li>
        <li><?= $this->Html->link(__('Liste des catégories'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('Nouvelle catégorie'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="categories view large-9 medium-8 columns content">
    <h3><?= h($category->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nom') ?></th>
            <td><?= h($category->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Description') ?></th>
            <td><?= h($category->description) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($category->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Parent') ?></th>
            <td><?= $this->Number->format($category->parent_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lft') ?></th>
            <td><?= $this->Number->format($category->lft) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Rght') ?></th>
            <td><?= $this->Number->format($category->rght) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Créé le :') ?></th>
            <td><?= h($category->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modifié le :') ?></th>
            <td><?= h($category->modified) ?></td>
        </tr>
    </table>
</div>
