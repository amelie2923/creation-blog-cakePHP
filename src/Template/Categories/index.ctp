<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface[]|\Cake\Collection\CollectionInterface $categories
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Nouvelle catégorie'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="categories index large-9 medium-8 columns content">
    <!-- <h3><?= __('Categories') ?></h3> -->
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th>Id</th>
                <th>Id Parent</th>
                <th>Lft</th>
                <th>Rght</th>
                <th>Nom</th>
                <th>Description</th>
                <th>Créé le : </th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categories as $category): ?>
            <tr>
                <td><?= $category->id ?></td>
                <td><?= $category->parent_id ?></td>
                <td><?= $category->lft ?></td>
                <td><?= $category->rght ?></td>
                <td><?= h($category->name) ?></td>
                <td><?= h($category->description) ?></td>
                <td><?= h($category->created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Voir'), ['action' => 'view', $category->id]) ?><br>
                    <?= $this->Html->link(__('Editer'), ['action' => 'edit', $category->id]) ?><br>
                    <?= $this->Form->postLink(__('Supprimer'), ['action' => 'delete', $category->id], ['confirm' => __('Êtes-vous sûr de vouloir supprimer # {0}?', $category->id)]) ?><br>
                    <?= $this->Form->postLink(__('Descendre'), ['action' => 'moveDown', $category->id], ['confirm' => __('Êtes-vous sûr de vouloir descendre # {0}?', $category->id)]) ?><br>
                    <?= $this->Form->postLink(__('Monter'), ['action' => 'moveUp', $category->id], ['confirm' => __('Êtes-vous sûr de vouloir monter # {0}?', $category->id)]) ?><br>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>