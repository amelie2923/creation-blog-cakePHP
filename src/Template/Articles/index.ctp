<!--Création de la vue pour l'action index que l'on a créé dans le controller Articles -->

<h1>Tous les articles du Blog</h1>
<?= $this->Html->link('Ajouter un article', ['action' => 'add'])?>
<table>
    <tr>
        <th>Id</th>
        <th>Titre</th>
        <th>Créé le :</th>
        <th>Actions</th>
    </tr>

<!--Ici se trouve l'itération sur l'objet query de nos $articles, l'affichage des infos des articles -->

<?php foreach ($articles as $article): ?>
    <tr>
        <td><?= $article->id ?></td>  <!--ID-->
        <td>
            <?= $this->Html->link($article->title, ['action' => 'view', $article->id]) ?>   <!--TITRE-->
            <!-- 
                $this->Html = instance de la classe CakePHP Cake\View\Helper\HtmlHelper 
                link() génèrera un lien HTML à partir d’un titre (1er paramètre) et d’une URL (2nd paramètre)
            -->
        </td>
        <td>
            <?= $article->created->format('d-m-Y') ?>   <!--CRÉÉ LE-->
        </td>
        <td>
            <?= $this->Form->postLink( //important d'utiliser le Post pour supprimer et non le Get à cause des robots d'indexation
                'Supprimer', 
                ['action' => 'delete', $article->id],
                ['confirm' => 'Etes-vous sûr de vouloir supprimer cet article?'])     
            ?> <!--SUPPRIMER--> 
            - 
            <?= $this->Html->link('Modifier', ['action' => 'edit', $article->id])?>   <!--MODIFIER-->
        </td>
    </tr>
<?php endforeach; ?>
</table>
