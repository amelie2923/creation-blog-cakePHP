<h1>Ajouter un article</h1>
<!--$this->Form = helper-->
<?php 
    echo $this->Form->create($article);  //correspond au html : <form method="post" action="/articles/add">
    echo $this->Form->control('category_id'); //ajout des input (via la methode "control") liés aux catégories
    echo $this->Form->control('title');  //$this->Form->control() = créer des elements de formulaire du même nom : 1er paramètre = champ correspondant, 2nd parametre = spécifier une option
    echo $this->Form->control('body', ['rows' => '3']);
    echo $this->Form->button(__("Sauvegarder l'article"));
    echo $this->Form->end(); //cloture le formulaire 
?>