<h1><?= h($article->title) ?></h1>  <!-- h() permet d'échapper les données-->
<p><?= h($article->body) ?></p>  
<p><small>Created : <?= $article->created->format(DATE_RFC850) ?><small>
